package com.evomag.automation.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

//the class defines the driver used for the tests, excepting the login and registration tests
public class DriverSetting {
    public void myDriver(){
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/");

        Utils.mySleeper(2000);

        WebElement backtosite = driver.findElement(By.xpath("html/body/div[1]/div[1]/div[1]/div[2]/div/div/a[1]"));
        if (backtosite.isDisplayed()){
            backtosite.click();
            Utils.mySleeper(3000);}
    }
}
