package com.evomag.automation.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//the driver for the login and registration tests
public class DriverSettingLogIn {
    public void myDriver() {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/client/auth");

        Utils.mySleeper(2000);
    }

}
