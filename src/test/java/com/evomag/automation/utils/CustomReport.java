package com.evomag.automation.utils;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.xml.XmlSuite;

import java.util.List;
import java.util.Map;

/**
 * Created by master on 11/6/2017.
 */
public class CustomReport implements IReporter {

    public void generateReport(List<XmlSuite> xmlDoc, List<ISuite> suits, String outDir) {
        for (ISuite ist : suits) {
            String suiteName = ist.getName();
            Map<String,ISuiteResult> results = ist.getResults();
            for (ISuiteResult isr : results.values()) {
                ITestContext itc = isr.getTestContext();
                System.out.println("FAILED : " + itc.getFailedTests().getAllResults().size());
                System.out.println("PASSED : " + itc.getPassedTests().getAllResults().size());
                System.out.println("SKIPPED : " + itc.getSkippedTests().getAllResults().size());
                System.out.println("TOTAL : " + itc.getAllTestMethods().length);
            }
        }
    }
}
