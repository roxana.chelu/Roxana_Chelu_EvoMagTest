package com.evomag.automation.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 10/10/2017.
 */
//the class defines the browsers used for the tests.
public class WebBrowser {
    public enum Browsers {
        FIREFOX,
        CHROME,
        IE,
        HTMLUNIT
    }

    public static WebDriver getDriver(Browsers browser) throws Exception {
        WebDriver driver = null;

        switch (browser) {
            case IE:
            {
                System.setProperty("webdriver.ie.driver", "src\\main\\resources\\drivers\\IEDriverServer32.exe");
                return new InternetExplorerDriver();
            }
            case CHROME:
            {
                System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");

                ChromeOptions options = new ChromeOptions();
                options.addArguments("start-maximized");
                return new ChromeDriver(options);
            }
            case FIREFOX:
            {
                System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
                ProfilesIni profile = new ProfilesIni();
                FirefoxProfile myprofile = profile.getProfile("default");


                DesiredCapabilities dc = new DesiredCapabilities();
                dc.setCapability(FirefoxDriver.PROFILE, myprofile);
                dc.setCapability("browserName","safari");

                return new FirefoxDriver(dc);
            }
            case HTMLUNIT:
            {
                return new HtmlUnitDriver();
            }
            default:
            {
                throw new Exception("Invalid browser");
            }
        }
    }
}
