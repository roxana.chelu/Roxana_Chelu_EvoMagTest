package com.evomag.automation.utils;

//the class used for taking the data for the test from an excel document. It is used to return the name, email, password, addrress, product type and wishlist name.
public class UsedData {
    private String username;
    private String password;
    private String email;
    private String newaddress;
    private String yourproduct;
    private String wishlistname;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username=username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password= password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email= email;
    }

    public String getNewAddress() {
        return newaddress;
    }

    public void setNewaddress(String newaddress) {
        this.newaddress= newaddress;
    }

    public String getproduct() {
        return yourproduct;
    }

    public void setProduct(String yourproduct) {
        this.yourproduct= yourproduct;
    }

    public String getWishlistName() {
        return wishlistname;
    }

    public void setWishlistName(String wishlistname) {
        this.wishlistname= wishlistname;
    }
}
