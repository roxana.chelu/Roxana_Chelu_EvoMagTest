package com.evomag.automation.utils;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by master on 10/24/2017.
 */
//the class starts and closes the driver
public class UtilsTest {

    public WebDriver driver = null;

    WebBrowser.Browsers browserType;

    @BeforeTest
    public void startDriver() {
        try {
            driver = WebBrowser.getDriver(WebBrowser.Browsers.FIREFOX);
        }
        catch (Exception ex) {
            System.out.println("Browser specified is not on the supported browser list! Please use firefox, chrome, ie or htmlunit as argument!");
        }
    }


    @AfterTest
    public void closeDriver() {
        try {
            driver.close();
        }
        catch (Exception ex) {
            System.out.println("No driver was to close!");
        }
    }
}



