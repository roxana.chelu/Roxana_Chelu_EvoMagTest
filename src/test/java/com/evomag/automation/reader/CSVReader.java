package com.evomag.automation.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {

    /**
     * Reads a CSV file line by line assuming that comma is the fields separator
     *
     * @param file The CSV file
     *
     * @return a list of String arrays (each file line is a list item, each array item is a line value)
     */
    public static List<String[]> readCSVFile(File file) {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";
        List<String[]> text = new ArrayList<String[]>();

        try {

            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                text.add(line.split(cvsSplitBy, -1));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return text;

    }
}
