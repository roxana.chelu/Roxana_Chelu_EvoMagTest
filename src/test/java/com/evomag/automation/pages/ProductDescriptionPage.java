package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//this class is used to display some information for a certain product
public class ProductDescriptionPage {
    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/div[2]/div[2]/div[5]/div[1]/div/div[3]/a")
    private WebElement phoneSelection;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div[7]/div[1]/ul/li[1]")
    private WebElement recomendedProducts;

    @FindBy(how = How.XPATH, using = ".//*[@id='ConsumabileAccesorii']/ul/li[4]")
    private WebElement externalCharges;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div[7]/div[1]/ul/li[4]")
    private WebElement technicalDescription;

    /*this method is used to open the recommended products category and to select external charges for a previously searched phone and then to open the technical description section.

    The steps are:
    1. Opens the selected phone
    2. Clicks the recommended products section
    3. Opens the external charges from the recommended product section
    4. Clicks the technical description section
    */
    public void myProductDescription() {
        phoneSelection.click();
        Utils.mySleeper(3000);
        recomendedProducts.click();
        Utils.mySleeper(3000);
        externalCharges.click();
        Utils.mySleeper(3000);
        technicalDescription.click();
        Utils.mySleeper(3000);
    }
}
