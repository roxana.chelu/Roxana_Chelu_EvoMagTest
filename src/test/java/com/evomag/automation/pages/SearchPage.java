package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 10/10/2017.
 */

//this class is used to search for a product
public class SearchPage extends UtilsTest {

    @FindBy(how = How.XPATH, using = ".//*[@id='searchString'][2]")
    private WebElement searchField;

    @FindBy(how = How.XPATH, using = ".//*[@id='top_search']/div/div[1]/input")
    private WebElement searchButtonField;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/div[2]/div[1]/div[4]/ul/li[1]/label/span[1]")
    private WebElement whiteColour;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div/div[2]/div[1]/div[6]/ul/li[3]/label/span[1]")
    private WebElement bikeType;

    /*this method is used to populate the search bar and to search for the written product.

    * @param youroption represents the searched product*/
    public void searchOptions(String youroption){
        Utils.mySleeper();
        searchField.clear();
        searchField.sendKeys(youroption);
        Utils.mySleeper(2000);
        searchButtonField.submit();
    }

   /*this method is used to select from the left side menu the colour of the bike. In addition, the price slider is adjusted, thus the maximum price is deminished.

   The steps are:
    1. from the left menu, sets the colour to white
    2. selects the bike type

     */
    public void bikeSelectionCharacteristics(){
        whiteColour.click();
        Utils.mySleeper(2000);
        bikeType.click();
        Utils.mySleeper(2000);
    }
}
