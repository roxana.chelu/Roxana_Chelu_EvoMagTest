package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 10/31/2017.
 */

//this class is used to add a product to the basket, to select a guarantee and then to delete the basket.
public class MyBasketPage {
    @FindBy(how = How.XPATH, using = ".//*[@id='CrossSaleProduct']/a")
    private WebElement seeDetails;

    @FindBy(how = How.XPATH, using = ".//*[@id='DepaneroWarranty_03596807']")
    private WebElement pickingGuarantee;

    @FindBy(how = How.XPATH, using = ".//*[@id='sendOrder-form']/table/tbody/tr[9]/td[1]/a[3]")
    private WebElement deleteBasket;

    //this method clicks the see details option for a searched product, selects the guarantee and empties the products.
    public void myBasket() {
        Utils.mySleeper(3000);
        seeDetails.click();
        Utils.mySleeper(3000);
        pickingGuarantee.click();
        Utils.mySleeper(5000);
        deleteBasket.click();
    }
}
