package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//this class is used to open the Iphone section, to select a phone and to add it to the basket
public class ProductSelectionPage {
    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[3]/div[2]/div/ul/li[1]/a")
    private WebElement offers;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div/div[3]/div[1]/div[1]/div/div[3]/a")
    private WebElement choice;

    @FindBy(how = How.XPATH, using = ".//*[@id='add-to-cart-detail']/div/div[3]/div[2]/div/div[1]/a")
    private WebElement addToBasket;


    @FindBy(how = How.CLASS_NAME, using = "ordinary-a add_to_wish_button")
    private WebElement addToWhishList;

    /*This method selects an Iphone and adds it to the basket.
    *
    * The steps are:
    * 1. Clicks the iphone 8 category
    * 2. Selects the first phone
    * 3. Clicks the add to the basket button
    * */
    public void selectOffers() {
        offers.click();
        Utils.mySleeper(1000);
        choice.click();
        Utils.mySleeper(1000);
        addToBasket.submit();
    }

    /*this method adds to wishlist an Iphone, after being searched in the Iphone 8 section

    The steps are:
    * 1. Clicks the iphone 8 category
    * 2. Selects the first phone
    * 3. Clicks the add to the basket button
     */
    public void addTowishlist() {
        offers.click();
        Utils.mySleeper(1000);
        choice.click();
        Utils.mySleeper(3000);
        addToWhishList.submit();
    }

}
