package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//this class searches in the laptops and PCs categories

public class CategoriesPage extends UtilsTest{

    @FindBy(how = How.XPATH, using = ".//*[@id='menu_catex']/div[2]/div/ul/li[1]/a")
    private WebElement cat1;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div/div[2]/ul/li[1]/a[2]")
    private WebElement subcat1;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div/div[2]/div[1]/div[3]/div/ul/li[2]/a")
    private WebElement appleLaptop;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div/div[2]/div[1]/div[1]/div[1]/a")
    private WebElement deleteFilter;


    /*the method searches in the Laptops category, opens the first category and selects the Apple laptops.

    The steps are:
    * 1. clicks the Laptopuri si tablete category
    * 2.clicks the Laptopuri subcategory
    * 3.from the left menu, selects the Apple producer
    * 4.deletes the Apple filter by clcking "Sterge filtre" option
    * */
    public void findingLaptops(){

        cat1.click();
        Utils.mySleeper(2000);
        subcat1.click();
        appleLaptop.click();
        Utils.mySleeper(3000);
        deleteFilter.click();
    }
}