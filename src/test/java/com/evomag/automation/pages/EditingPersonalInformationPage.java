package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//this class identifies some fields from the personal information page, i.e. the gender radio button(genderField), the email field, the new address field and the save button, to change the already existing information
public class EditingPersonalInformationPage {
    @FindBy(how = How.XPATH, using = ".//*[@id='Partner_GenderId_0']")
    private WebElement genderField;

    @FindBy(how = How.XPATH, using = ".//*[@id='Partner_Email']")
    private WebElement emailField;

    @FindBy(how = How.XPATH, using = ".//*[@id='addAddress']")
    private WebElement newAddress;

    @FindBy(how = How.XPATH, using = ".//*[@id='PartnerAddress_Address']")
    private WebElement myNewAddress;

    @FindBy(how = How.XPATH, using = ".//*[@id='save']")
    private WebElement saveChanges;

    /*this method is used to change the gender, to add a new email and a new address to the account information page
    *
    * @param email represents the new email address
    *
    * @param address represents the new address
    * */
    public void changePersonalInfo(String email, String address){
        genderField.click();
        emailField.clear();
        emailField.sendKeys(email);
        Utils.mySleeper(2000);
        newAddress.click();
        Utils.mySleeper(2000);
        myNewAddress.clear();
        myNewAddress.sendKeys(address);
        Utils.mySleeper(3000);
        saveChanges.click();

    }
}
