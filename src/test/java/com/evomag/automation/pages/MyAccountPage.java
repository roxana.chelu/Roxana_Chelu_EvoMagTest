package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//this class is used to open the account page, more exactly to open the account details and the previous ordered products list.
public class MyAccountPage extends UtilsTest{
    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[2]/div[1]/a")
    private WebElement accountDetails;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div[2]/ul/li[4]")
    private WebElement ordersList;

    @FindBy(how = How.XPATH, using = ".//*[@id='yw0']/table/tbody/tr/td[6]/a")
    private WebElement myOrder;


    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[2]/div[13]/a")
    private WebElement logOut;

    //this method hovers over My account and selects the account details option
    public void myAccountDetails(){
        WebElement me = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[1]/em"));
        Actions actions1 = new Actions(driver);
        actions1.moveToElement(me).build().perform();
        Utils.mySleeper(3000);
        accountDetails.click();
        Utils.mySleeper(3000);
    }

    /*this method hovers over My account, selects the account details and then opens the previous ordered products list. Then opens an order to see the details
    *
    * The steps are:
    * 1. hovers over Bine ai venit
    * 2. Selects Detalii cont
    * 3. Selects Istoric comenzi
    * 4. Opens my order
    * */
    public void MyOrders(){
       /* WebElement me = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[1]/em"));
        Actions actions1 = new Actions(driver);
        actions1.moveToElement(me).build().perform();
        Utils.mySleeper(3000);
        accountDetails.click();*/
        Utils.mySleeper(3000);
        ordersList.click();
        Utils.mySleeper(3000);
        myOrder.click();
    }

    //this method is used to log out from the account, by hovering over My account and selecting log out option.
    public void logOut(){
        WebElement me = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[1]/em"));
        Actions actions1 = new Actions(driver);
        actions1.moveToElement(me).build().perform();
        Utils.mySleeper(3000);
        logOut.click();

    }

}