package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 10/10/2017.
 */
//after adding a certain product to the wishlist this class populates the wishlist name and saves the wishlist list
public class WishlistPage extends UtilsTest {


    @FindBy(how = How.ID, using = "Wishlist_Name")
    private WebElement nameField;

    @FindBy(how = How.ID, using = "Wishlist_VisibleForAll")
    private WebElement whislistVisiblecheckboxField;

    @FindBy(how = How.CLASS_NAME, using = "butn_form")
    private WebElement saveField;

   /*this method populates the wishlist name

   * @param yourWishlistName represents the name selected for the wishlist
   * */
    public void wishlist(String yourWishlistName){
        Utils.mySleeper();
        nameField.clear();
        nameField.sendKeys(yourWishlistName);
        Utils.mySleeper(2000);
        whislistVisiblecheckboxField.click();
        saveField.submit();
    }
}
