package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//this class is used to open the used products section, to select Casti Bluetooth si audio  and to randomly sort the displayed products according to price, relevance,etc
public class ReusedPage extends UtilsTest{
    @FindBy(how = How.XPATH, using = ".//*[@id='menu_catex']/div[2]/div/ul/li[20]/a")
    private WebElement reusedCategory;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div/div[2]/div[2]/div[1]/ul/li[4]/a")
    private WebElement allSection;

   /* @FindBy(how = How.XPATH, using = " html/body/div[5]/div[4]/div[1]/div/div[2]/div[1]/div[1]/div[43]/a")
    private WebElement productHeadset;*/

    //this method opens the used products category and checks if there are any Bluetooth headsets in this section. Then it randomly sorts the product
    public void reusedSelection() {
        reusedCategory.click();
        Utils.mySleeper(2000);
        allSection.click();
    }

}
