package com.evomag.automation.pages;

import com.evomag.automation.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 10/10/2017.
 */

//this class is used to identify the email and password fields, necessary to log in, and the gender, full name, email fields, necessary to create a new account
public class LoginPage {
    @FindBy(how = How.ID, using = "LoginClientForm_Email")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "LoginClientForm_Password")
    private WebElement passField;

    @FindBy(how = How.XPATH, using = "html/body/div[5]/div[4]/div[1]/div[2]/div/div[2]/div/div/div[3]/input")
    private WebElement loginButtonField;

    @FindBy(how = How.ID, using = "showClassDialog")
    private WebElement forgotPasswordField;

    @FindBy(how = How.ID, using = "RegisterClientForm_Gender_0")
    private WebElement genderField;

    @FindBy(how = How.ID, using = "RegisterClientForm_FullName")
    private WebElement FullNameField;

    @FindBy(how = How.ID, using = "RegisterClientForm_Email")
    private WebElement EmailRegistrationField;

    @FindBy(how = How.ID, using = "RegisterClientForm_Password")
    private WebElement PassRegistrationField;

    @FindBy(how = How.ID, using = "RegisterClientForm_newsletterMe")
    private WebElement NewsletterField;

    @FindBy(how = How.CLASS_NAME, using = "butn_form")
    private WebElement SubscribeField;


    /*this method is used to populate the log in section with the email and password.

    * @param email represents the email used to log in
    *
    * @param password represents the password to log in*/
    public void login(String email, String password) {
        emailField.clear();
        emailField.sendKeys(email);
        Utils.mySleeper(1000);
        passField.clear();
        passField.sendKeys(password);
        loginButtonField.click();
        Utils.mySleeper(1000);
    }

    /*this method is used to populate the registration section with the name, email address and password to create a new account.

    * @param name represents the new user's name
    *
    * @param email represents the user's email
    *
    * @param password represents the user's password
    * */
    public void registration(String name, String email, String password) {
        FullNameField.clear();
        FullNameField.sendKeys(name);
        Utils.mySleeper(2000);
        PassRegistrationField.clear();
        PassRegistrationField.sendKeys(password);
        Utils.mySleeper(2000);
        EmailRegistrationField.clear();
        EmailRegistrationField.sendKeys(email);
        Utils.mySleeper(2000);
        genderField.click();
        Utils.mySleeper(2000);
        NewsletterField.click();
        Utils.mySleeper(2000);
        SubscribeField.click();

    }



}
