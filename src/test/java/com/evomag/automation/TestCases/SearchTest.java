package com.evomag.automation.TestCases;

import com.evomag.automation.pages.SearchPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 10/24/2017.
 */
//this class tests the search function.
public class SearchTest extends UtilsTest {
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider(){
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add (new String[]{"bicicleta"});
        return dp.iterator();
    }

    /*the method searches for a certain product and then selects the product based on two criteria: white colour and price

    * @param youroption represents the searched product
    *
    * @see SearchPage for detailed steps
    * */
    @Test(dataProvider = "LoginDataProvider")
    public void searchTest(String youroption) {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/");

        Utils.mySleeper(2000);

        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        searchPage.searchOptions(youroption);
        Utils.mySleeper(2000);
        searchPage.bikeSelectionCharacteristics();

        //from the left menu, increses and diminishes the price used for search
        Actions move = new Actions(driver);
        WebElement slider = driver.findElement(By.className("slider-button"));
        move.clickAndHold(slider).moveByOffset(50, 50).build().perform();
        Utils.mySleeper(3000);
    }
}
