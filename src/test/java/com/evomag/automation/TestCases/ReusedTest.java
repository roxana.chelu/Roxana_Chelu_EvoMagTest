package com.evomag.automation.TestCases;

import com.evomag.automation.pages.ReusedPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by master on 10/31/2017.
 */
//this class tests the used products category. First, it opens the section, selects a certain category and then randomly sorts the products
public class ReusedTest extends UtilsTest {

    /*the method selects the used Products category.
    *
    * The steps are:
    * 1. opens the category to select Produse resigilate
    * 2. clicks the all subcategory
    * 3. checks if there are any all in one PC products in this category
    * 3. if any, randomly sorts the found products
    * */
    @Test
    public void usedProductsCategories() {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/");

        Utils.mySleeper(2000);

        ReusedPage myUsedProduct = PageFactory.initElements(driver, ReusedPage.class);
        myUsedProduct.reusedSelection();
        Utils.mySleeper(2000);

        WebElement allInOnePC = driver.findElement(By.xpath("html/body/div[5]/div[4]/div[1]/div/div[2]/div[1]/div[1]/div[5]/a"));
        if (allInOnePC.isDisplayed()){
            allInOnePC.click();
            Utils.mySleeper(2000);
            WebElement sortingSection = driver.findElement(By.xpath(".//*[@id='sortWidget']"));
            Actions act = new Actions(driver);
            act.click(sortingSection).build().perform();
            int randVal = (int)Math.random()* 6 + 1;
            WebElement sortingItem = driver.findElement(By.xpath(".//*[@id='sortWidget']/option[" + randVal + "]"));
            sortingItem.click();
        } else{
            System.out.print("There aren't any products in this category");}
    }

}
