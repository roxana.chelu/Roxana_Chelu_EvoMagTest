package com.evomag.automation.TestCases;

import com.evomag.automation.pages.ProductDescriptionPage;
import com.evomag.automation.pages.SearchPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 10/31/2017.
 */
//this class tests the product description section. It searches for a product, using the SearchPage and then opens the recommended products category, selects external charges for a previously searched phone and opens the technical description section;
public class ProductDescriptionTest extends UtilsTest {
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"samsung s7 edge"});
        return dp.iterator();
    }

   /*tests the product description section.

   The steps are:
   1. searches a product using the SearchOptions method from the SearchPage
   2. opens the product details  using the ProductDescriptionPage

   * @param desired product is the product used for opening the description section
   *
   * @see SearchPage for testing steps
   *
   * @see ProductDescriptionPage for testing steps
   * */
    @Test(dataProvider = "LoginDataProvider")
    public void productTest(String desiredProduct) {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/");

        Utils.mySleeper(2000);

        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        searchPage.searchOptions(desiredProduct);
        Utils.mySleeper(2000);


        ProductDescriptionPage myProduct = PageFactory.initElements(driver, ProductDescriptionPage.class);
        myProduct.myProductDescription();
        Utils.mySleeper(2000);
    }
}


