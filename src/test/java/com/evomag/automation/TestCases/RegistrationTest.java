package com.evomag.automation.TestCases;

/**
 * Created by master on 10/24/2017.
 */
import com.evomag.automation.pages.LoginPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

//this class tests the registration section, trying to create a new user
public class RegistrationTest extends UtilsTest {
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"Name", "", ""});
        dp.add(new String[]{"", "Email", ""});
        dp.add(new String[]{"", "", "Password"});
        dp.add(new String[]{"Name", "Email", ""});
        dp.add(new String[]{"Name", "", "Password"});
        dp.add(new String[]{"", "Email", "Password"});
        dp.add(new String[]{"Name", "Password", "Email"});
        return dp.iterator();
    }


    /*this method uses the registration method from the LoginPage to create a new user

    * @param name is the new user name
    *
    * @param email is the new user's email
    *
    * @param password is the new user's password
    * */
    @Test(dataProvider = "LoginDataProvider")
    public void registrationTest(String name, String email, String password) {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/client/auth");

        Utils.mySleeper();

        LoginPage registrationPage = PageFactory.initElements(driver, LoginPage.class);
        registrationPage.registration(name, email, password);

        try{
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch(Exception ex){
            System.out.println("No message box");
        }

    }
}
