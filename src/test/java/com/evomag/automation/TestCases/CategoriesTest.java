package com.evomag.automation.TestCases;

import com.evomag.automation.pages.CategoriesPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by master on 10/24/2017.
 */
//this class uses the CategoryPage class to search in the categories section
public class CategoriesTest extends UtilsTest {
    /*this test opens the laptop category from the Categories section and selects one subcategory
    *
    * @see CategoryPage for the detailed steps
    * */
    @Test
    public void laptopCategories() {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/");

        CategoriesPage myLaptop = PageFactory.initElements(driver, CategoriesPage.class);
        myLaptop.findingLaptops();
    }

    /*this test opens the PCs category from the Categories section and then selects one subcategory
    *
    * The steps are:
    * 1. hovers over the Sisteme PC category
    * 2. clicks the Branduri subcategory
    * 3. from the left menu, randomly selects a processor speed(additional criteria area)
    * */
    @Test
    public void pcCategories(){
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/");

        Utils.mySleeper(2000);

        Actions actions1 = new Actions(driver);
        WebElement hoverButton1 = driver.findElement(By.xpath(".//*[@id='menu_catex']/div[2]/div/ul/li[2]/a"));
        System.out.println("This is the hover button 1 pointer " + hoverButton1.toString());
        actions1.moveToElement(hoverButton1).build().perform();
        Utils.mySleeper(3000);
        WebElement item = driver.findElement(By.xpath(".//*[@id='cat91']/div[1]/ul/li[1]/a"));
        item.click();

        Utils.mySleeper(3000);

        Actions act = new Actions(driver);
        WebElement processorSpeedCriteria = driver.findElement(By.id("sortWidget"));
        act.click(processorSpeedCriteria).build().perform();
        int randVal = (int)Math.random()* 27 + 1;
        WebElement sortingItem = driver.findElement(By.xpath(".//*[@id='Specification_11360']/option[" + randVal + "]"));
        sortingItem.click();
    }
}
