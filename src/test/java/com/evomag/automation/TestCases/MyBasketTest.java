package com.evomag.automation.TestCases;

import com.evomag.automation.pages.MyBasketPage;
import com.evomag.automation.pages.ProductSelectionPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by master on 10/31/2017.
 */
//this class adds a phone to tha basket. First, searches for an iphone, then selects the phone and adds it to the basket. In addition, it selects a guarantee
public class MyBasketTest extends UtilsTest {

    /*this method tests the basket functionality.
    * The steps are:
    * 1. selects an iphone using the selectOffers method
    * 2. ads the phone to the basket using myBasket method
    *
    * @see ProductSelectionPage for detailed steps
    * @see MyBasketPage for detailed steps
    * */
    @Test
    public void myBasketTest() {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/");

        Utils.mySleeper(2000);

        ProductSelectionPage mySelectionTest = PageFactory.initElements(driver, ProductSelectionPage.class);
        mySelectionTest.selectOffers();

        Utils.mySleeper(2000);

        MyBasketPage myBaskettest = PageFactory.initElements(driver, MyBasketPage.class);
        myBaskettest.myBasket();
    }
}

