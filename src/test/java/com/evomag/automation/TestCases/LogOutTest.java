package com.evomag.automation.TestCases;

import com.evomag.automation.pages.LoginPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

//this class tests the log out issue, by logging in the user, opening the account details and clicking the log out option
public class LogOutTest extends UtilsTest{
    @Test
    public void loggingOut() {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/client/auth");

        Utils.mySleeper(2000);

        LoginPage loginTest = PageFactory.initElements(driver, LoginPage.class);
        loginTest.login("roxana.chelu@yahoo.com", "alabala87");

        Utils.mySleeper(2000);

        WebElement me = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[1]/em"));
        Actions actions1 = new Actions(driver);
        actions1.moveToElement(me).build().perform();
        Utils.mySleeper(3000);
        WebElement logOut = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[2]/div[13]/a"));
        logOut.click();

        /*MyAccountPage exitAccount = PageFactory.initElements(driver, MyAccountPage.class);
        Utils.mySleeper();
        exitAccount.logOut();*/
    }

}
