package com.evomag.automation.TestCases;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

/**
 * Created by master on 11/6/2017.
 */
//this class is used for reporting
public class TestReportingTest {
    ExtentReports ex =  new ExtentReports("C:\\report.html",false);

    @Test()
    public void test1() {
        ExtentTest t1 = ex.startTest("LoginPageTest");
        System.out.println("Test 1 launched");
        t1.log(LogStatus.PASS,"login");
        ex.endTest(t1);
        ex.flush();
    }
}
