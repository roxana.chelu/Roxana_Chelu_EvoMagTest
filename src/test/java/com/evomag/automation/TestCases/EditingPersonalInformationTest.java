package com.evomag.automation.TestCases;

import com.evomag.automation.pages.EditingPersonalInformationPage;
import com.evomag.automation.pages.LoginPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

//this class tests if the personal information can be changed.
public class EditingPersonalInformationTest extends UtilsTest{
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"rox.chelu@yahoo.com", "Piata Unirii"});
        return dp.iterator();
    }

    /*this method logs in on the website, opens the account details and then changes the gender, address and email, using the LoginPage, MyAccountPage and EditingPersonalInformationPag

    The steps are:
    1. logs in with the given credentials
    2. opens my details page using the myDetails method from MyAccountPage
    3. changes the gender, adds a new email and a new address to the account

    * @param email is the new email address
    *
    * @param address is the new address
    *
    * @see for more information see the MyAccountPage and EditingPersonalInformationPage
    * */
    @Test(dataProvider = "LoginDataProvider")
    public void editingTest(String email, String address) {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/client/auth");

        Utils.mySleeper();

        LoginPage loginTest = PageFactory.initElements(driver, LoginPage.class);
        loginTest.login("roxana.chelu@yahoo.com", "alabala87");

        Utils.mySleeper(2000);

        WebElement me = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[1]/em"));
        Actions actions1 = new Actions(driver);
        actions1.moveToElement(me).build().perform();
        Utils.mySleeper(3000);
        WebElement acountDetails = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[2]/div[1]/a"));
        acountDetails.click();

        //MyAccountPage myDetails = PageFactory.initElements(driver, MyAccountPage.class);
        //myDetails.myAccountDetails();

        Utils.mySleeper(2000);

        EditingPersonalInformationPage myEdit = PageFactory.initElements(driver, EditingPersonalInformationPage.class);
        myEdit.changePersonalInfo(email, address);
        Utils.mySleeper(2000);
    }
}
