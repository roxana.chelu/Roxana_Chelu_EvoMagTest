package com.evomag.automation.TestCases;

import com.evomag.automation.pages.LoginPage;
import com.evomag.automation.pages.ProductSelectionPage;
import com.evomag.automation.pages.WishlistPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 10/31/2017.
 */
//the class tests the wishlist option.
public class WishlistPageTest extends UtilsTest {
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider(){
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add (new String[]{"my Christmas wish"});
        return dp.iterator();
    }

    /*The method searches a certain product, using the ProductSelectionPage and then adds it to the wishlist.

    The steps are:
    1. logs in
    2. using the addTowishlist method, selects the product
    3. adds the product to the wishlist
    4. sets the wishlist name

    * @param yourWislisthName represents the name assigned to the created wishlist
    *
    * @see WishlistPage for detailed steps
    * @see ProductSelectionPage for detailed steps
    * */
    @Test(dataProvider = "LoginDataProvider")
    public void wishlistTest(String yourWishlistName) {
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/client/auth");


        LoginPage loginTest = PageFactory.initElements(driver, LoginPage.class);
        loginTest.login("roxana.chelu@yahoo.com", "alabala87");

        Utils.mySleeper(2000);

        ProductSelectionPage myProduct = PageFactory.initElements(driver, ProductSelectionPage.class);
        myProduct.addTowishlist();
        Utils.mySleeper(2000);

        WishlistPage wishlistPage = PageFactory.initElements(driver, WishlistPage.class);
        wishlistPage.wishlist(yourWishlistName);
        Utils.mySleeper(2000);
    }
}
