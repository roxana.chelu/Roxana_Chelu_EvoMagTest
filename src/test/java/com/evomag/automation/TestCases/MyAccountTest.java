package com.evomag.automation.TestCases;


import com.evomag.automation.pages.LoginPage;
import com.evomag.automation.pages.MyAccountPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/*this class tests the account details: logs in and then opens my account and also my previous ordered products list
*
* @see MyAccountPage for testing steps
* */
public class MyAccountTest extends UtilsTest{

    /*the method tests the account section, by logging in to the account, opening the details and furthermore opening the list with the ordered products
    *
    * The steps are:
    * 1. logs in
    * 2. hovers over my account and opens details
    * 3. clicks my history/previous ordered products
    * 4. opens the details for my order*/
    @Test
    public void myAccount() {

        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/client/auth");

        Utils.mySleeper(2000);

        LoginPage loginTest = PageFactory.initElements(driver, LoginPage.class);
        loginTest.login("roxana.chelu@yahoo.com", "alabala87");

        Utils.mySleeper(2000);


        WebElement me = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[1]/em"));
        Actions actions1 = new Actions(driver);
        actions1.moveToElement(me).build().perform();
        Utils.mySleeper(3000);
        WebElement acountDetails = driver.findElement(By.xpath(".//*[@id='personal_header']/div[2]/div[2]/div[1]/a"));
        acountDetails.click();

        MyAccountPage myAccount = PageFactory.initElements(driver, MyAccountPage.class);
        Utils.mySleeper();
        myAccount.MyOrders();
    }
}
