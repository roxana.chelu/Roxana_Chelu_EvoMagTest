package com.evomag.automation.TestCases;

import com.evomag.automation.pages.LoginPage;
import com.evomag.automation.utils.Utils;
import com.evomag.automation.utils.UtilsTest;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 10/17/2017.
 */
//this class is used to test the log in. There are 3 scenarios, one with the email address, one with the password and one with both email and password.
public class LoginPageTest extends UtilsTest {
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider(){
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add (new String[]{"email", ""});
        dp.add (new String[]{"", "password"});
        dp.add (new String[]{"email", "password"});
        dp.add (new String[]{"", ""});
        dp.add (new String[]{"roxana.chelu@yahoo.com", "alabala87"});
        return dp.iterator();
    }

    /*this method tests the login connection

    * @param  email is the email address used to connect
    * @param password is the password used to connect
    * */
    @Test (dataProvider = "LoginDataProvider")
    public void loginTest(String email, String password){
        System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.evomag.ro/client/auth");

        Utils.mySleeper(2000);

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Utils.mySleeper();
        loginPage.login(email, password);

        try{
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch(Exception ex){
            System.out.println("No message box");
        }
    }


}
